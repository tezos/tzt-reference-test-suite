This repository has been archived, the TZT reference test suite now
lives [on the tezos/tezos Gitlab
repository](https://gitlab.com/tezos/tezos/-/tree/master/tzt_reference_test_suite).
